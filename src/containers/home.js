import React from 'react'
import { connect } from 'react-redux'
import HomePage from '../components/templates/home-page'
import {
	createPost,
	fetchAllPosts,
	fetchSinglePost,
	cleanState,
} from '../actions/index'

const HomeContainer = ({
	todolist,
	createPost,
	fetchAllPosts,
	singlePost,
	fetchSinglePost,
	cleanState,
}) => (
	<HomePage
		todolist={todolist}
		createPost={createPost}
		fetchAllPosts={fetchAllPosts}
		singlePost={singlePost}
		fetchSinglePost={fetchSinglePost}
		cleanState={cleanState}
	/>
)

const mapStateToProps = state => ({
	todolist: state.todolist,
	singlePost: state.singlePost,
})

const mapDispatchToProps = dispatch => ({
	createPost: (title, description, priority, tags) => {
		dispatch(createPost(title, description, priority, tags))
	},
	fetchAllPosts: () => {
		dispatch(fetchAllPosts())
	},
	fetchSinglePost: id => {
		dispatch(fetchSinglePost(id))
	},
	cleanState: () => {
		dispatch(cleanState())
	},
})

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(HomeContainer)
