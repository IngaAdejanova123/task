import React from 'react'
import HomeContainer from './containers/home'

const App = () => {
	return <HomeContainer />
}

export default App
