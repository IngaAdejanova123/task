import axios from 'axios'
import { BASE_URI } from '../constants'
import { CLEAR_STATE, FETCH_SINGLE_POST } from './actionTypes'

export const cleanState = () => {
	return {
		type: CLEAR_STATE,
	}
}

export const fetchSinglePostSuccess = singlePost => {
	return {
		type: FETCH_SINGLE_POST,
		singlePost,
	}
}

export const fetchSinglePost = artistId => {
	return dispatch => {
		return axios
			.get(`${BASE_URI}/todos/${artistId}`)
			.then(response => {
				dispatch(fetchSinglePostSuccess(response.data))
			})
			.catch(error => {
				console.log(error) //or dispatch action that handles error behavior
			})
	}
}
