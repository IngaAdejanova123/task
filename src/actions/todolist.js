import axios from 'axios'
import { BASE_URI } from '../constants'
import { ADDING_POST_FETCH_ERROR, ADD_POST, FETCH_POST } from './actionTypes'

export const fetchingCreatePostError = error => {
	return {
		type: ADDING_POST_FETCH_ERROR,
		error,
	}
}

export const createPostSuccess = data => {
	const { id, createdAt, title, priority, isDone } = data

	return {
		type: ADD_POST,
		payload: {
			id,
			createdAt,
			title,
			priority,
			isDone,
		},
	}
}

export const createPost = (title, description, priority, tags) => {
	return dispatch => {
		return axios
			.post(`${BASE_URI}/todos`, { title, description, priority, tags })
			.then(response => {
				dispatch(createPostSuccess(response.data))
			})
			.catch(error => {
				dispatch(fetchingCreatePostError(error.response.data.error.message))
			})
	}
}

export const fetchPostsSuccess = todolist => {
	return {
		type: FETCH_POST,
		todolist,
	}
}

export const fetchAllPosts = () => {
	return dispatch => {
		return axios
			.get(`${BASE_URI}/todos`)
			.then(response => {
				dispatch(fetchPostsSuccess(response.data))
			})
			.catch(error => {
				console.log(error) // // dispatch action that handles error behavior
			})
	}
}
