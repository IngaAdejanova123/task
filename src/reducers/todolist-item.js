import { FETCH_SINGLE_POST, CLEAR_STATE } from '../actions/actionTypes'

export default (state = {}, action) => {
	switch (action.type) {
		case FETCH_SINGLE_POST:
			return {
				...state,
				...action.singlePost,
			}
		case CLEAR_STATE:
			return {}
		default:
			return state
	}
}
