import { combineReducers } from 'redux'
import todolist from './todolist'
import singlePost from './todolist-item'

export default combineReducers({
	todolist,
	singlePost,
})
