import React, { useEffect } from 'react'
import CreatePostForm from '../../blocks/create-post-form'
import TodoListItem from '../../blocks/todo-list-item'
import Header from '../../blocks/header'
import styles from './home-page.scss'

const HomePage = ({
	createPost,
	todolist,
	fetchAllPosts,
	resetPosts,
	fetchSinglePost,
	singlePost,
	cleanState,
}) => {
	useEffect(() => {
		fetchAllPosts()
	}, [])

	return (
		<>
			<Header />
			<div className={styles.container}>
				<CreatePostForm
					createPost={createPost}
					fetchAllPosts={fetchAllPosts}
					resetPosts={resetPosts}
					todoItemError={todolist.fetchErrorMessage}
				/>
				{(todolist.items || []).map(element => (
					<TodoListItem
						key={element.id}
						todoItem={element}
						fetchAllPosts={fetchAllPosts}
						fetchSinglePost={fetchSinglePost}
						singlePost={singlePost}
						cleanState={cleanState}
					/>
				))}
			</div>
		</>
	)
}

export default HomePage
