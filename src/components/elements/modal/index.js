import React from 'react'
import PropTypes from 'prop-types'

import Icon from '../../elements/icons'

import styles from './modal.scss'

export const Modal = ({ children, cleanState, handleClose }) => {
	const closeModal = () => {
		cleanState()
		handleClose()
	}

	return (
		<div className={styles.root}>
			<div className={styles.children}>
				{children}
				<button onClick={() => closeModal()} className={styles.close}>
					<Icon type="close" />
				</button>
			</div>
		</div>
	)
}

Modal.propTypes = {
	children: PropTypes.element,
	cleanState: PropTypes.func,
	handleClose: PropTypes.func,
}

export default Modal
