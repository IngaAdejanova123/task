#Component structure

The folder structure has been applied to divide up many of the components into groups to help facilitate understanding of their size and purpose within the application. The groups used are:
1)Elements, the smallest individual components that fit together into,
for example, (reusable button, icons(svg -> ..), modal, etc.)
2)Blocks, which group elements together into small, reusable components.
4)Templates, which represent the different "screens"

If project would be bigger , I would prefer to go with Atomic data structure (atoms, molecules, organisms, templates) instead of (elements, blocks, templates)
