import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

const TodolistItemModalContent = ({
	fetchSinglePost,
	singlePost,
	todoItemId,
}) => {
	useEffect(() => {
		fetchSinglePost(todoItemId)
	}, [])

	return (
		<>
			<h1>{singlePost.title}</h1>
			<p>{singlePost.description}</p>
		</>
	)
}

TodolistItemModalContent.propTypes = {
	todoItemId: PropTypes.string,
	singlePost: PropTypes.object,
	fetchSinglePost: PropTypes.func,
}

export default TodolistItemModalContent
