import React from 'react'
import styles from './header.scss'

const Header = () => {
	return (
		<header className={styles.root}>
			<div className={styles.image}>
				<img src="https://static.pi-top.com/images/logo/black-344x140.png" />
			</div>
		</header>
	)
}

export default Header
