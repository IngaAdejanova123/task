import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Modal from '../../elements/modal'
import styles from './todo-list-item.scss'
import axios from 'axios'
import { BASE_URI } from '../../../constants'
import TodolistItemModalContent from '../../blocks/todolist-item-modal-content'

const TodoListItem = ({
	fetchSinglePost,
	singlePost,
	todoItem,
	cleanState,
}) => {
	const [isSingleModalOpen, setIsSingleModalOpen] = useState(false)
	const [isTodoItemDone, setIsTodoItemDone] = useState(todoItem.isDone)
	const todoItemDate = new Date(todoItem.createdAt)

	const completeTodo = e => {
		setIsTodoItemDone(e.target.checked)

		axios
			.put(`${BASE_URI}/todos/${todoItem.id}`, { isDone: !isTodoItemDone })
			.catch(error => {
				console.log(error)
			})
	}

	const todoItemStatus = isTodoItemDone ? 'Task is Done!' : 'Task is not Done!'

	return (
		<div className={styles.root}>
			<input
				type="checkbox"
				checked={isTodoItemDone}
				onChange={e => completeTodo(e)}
			/>
			<div
				className={styles.info}
				onClick={() => setIsSingleModalOpen(!isSingleModalOpen)}
			>
				<h3>
					{todoItem.title} ({todoItem.priority}) - {todoItemStatus}
				</h3>
				<p>{todoItemDate.toString()}</p>
			</div>
			{isSingleModalOpen && (
				<Modal
					isSingleModalOpen={isSingleModalOpen}
					handleClose={() => setIsSingleModalOpen(!isSingleModalOpen)}
					cleanState={cleanState}
				>
					<TodolistItemModalContent
						fetchSinglePost={fetchSinglePost}
						singlePost={singlePost}
						todoItemId={todoItem.id}
					/>
				</Modal>
			)}
		</div>
	)
}

TodoListItem.propTypes = {
	cleanState: PropTypes.func,
	singlePost: PropTypes.object,
	todoItem: PropTypes.object,
	fetchSinglePost: PropTypes.func,
}

export default TodoListItem
