import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styles from './create-post-form.scss'
import axios from 'axios'
import { BASE_URI } from '../../../constants'

const CreatePostForm = ({ createPost, fetchAllPosts, todoItemError }) => {
	const [title, setTitle] = useState('')
	const [description, setDescription] = useState('')
	const [priority, setPriority] = useState(0)
	const [tags, setTags] = useState([])

	const resetPosts = async () => {
		await axios.post(`${BASE_URI}/reset`).catch(error => {
			console.log(error)
		})
		fetchAllPosts()
	}

	const tagHandleChange = e => {
		const value = Array.from(e.target.selectedOptions, option => option.value)
		setTags(value)
	}

	const submitPostForm = () => {
		createPost(title, description, priority, tags)
		setTitle('')
		setDescription('')
		setTags([])
		setPriority(0)
	}

	const getTagOptions = () => {
		const tagOptions = ['power', 'dairy', 'shopping']

		return (
			<select
				className={styles.inputField}
				multiple={true}
				value={tags}
				onChange={e => tagHandleChange(e)}
			>
				{tagOptions.map(element => (
					<option key={element} value={element}>
						{element}
					</option>
				))}
			</select>
		)
	}

	return (
		<>
			<div className={styles.root}>
				<input
					placeholder="title"
					value={title}
					onChange={e => setTitle(e.target.value)}
					className={styles.inputField}
					type="text"
				/>
				<input
					placeholder="description"
					value={description}
					onChange={e => setDescription(e.target.value)}
					className={styles.inputField}
					type="text"
				/>
				<input
					type="number"
					value={priority}
					onChange={e => setPriority(Number(e.target.value))}
					className={styles.inputField}
				/>
				{getTagOptions()}
				<button
					className={styles.addTodoButton}
					onClick={() => submitPostForm()}
				>
					Add new task
				</button>
				<button className={styles.seearchButton} onClick={() => resetPosts()}>
					Reset Todos
				</button>
			</div>
			<h3 className={styles.error}>{todoItemError}</h3>
		</>
	)
}

CreatePostForm.propTypes = {
	createPost: PropTypes.func,
	todoItemError: PropTypes.string,
	fetchAllPosts: PropTypes.func,
}

export default CreatePostForm
